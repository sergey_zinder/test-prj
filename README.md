
# PayMe Test Project 

- This project logic mostly implemented in [SaleController.php](https://bitbucket.org/sergey_zinder/test-prj/src/master/app/Http/Controllers/SaleController.php) controller
- Eloquent Model file can be found [here](https://bitbucket.org/sergey_zinder/test-prj/src/master/app/Sales.php)
- Several blade templates can be found [here](https://bitbucket.org/sergey_zinder/test-prj/src/master/resources/views/)
- In order to be able to run this project You have to put the proper configuration into Your .env file and run DB migrations
