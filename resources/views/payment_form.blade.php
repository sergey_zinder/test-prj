@extends('layout')
@section('title', 'Payment Form')

@section('assets')
    <link rel="stylesheet" href="./css/styles.css">
    <script src="https://cdn.paymeservice.com/hf/v1/hostedfields.js"></script>
@endsection
@section('content')
    <div class="first-example">
        <div class="justify-content-md-center">
            <div class="wrap-block">
                <!-- CREDIT CARD FORM STARTS HERE -->
                <form role="form" id="checkout-form" class="animated">

                    <div class="row-wrap-for-all-input">
                        <div class="one-block-for-all-divided-input">

                            <div class="wrap-for-input">
                                <div class="input-group" id="first-name-group">
                                    <div class="input-group-prepend">
                                        <label for="first-name-input" class="input-group-text">First Name</label>
                                    </div>
                                    <div id="first-name-container" class="form-control">
                                        <input id="first-name-input" type="text" placeholder="First name">
                                    </div>
                                </div>
                            </div>

                            <div class="wrap-for-input">
                                <div class="input-group" id="last-name-group">
                                    <div class="input-group-prepend">
                                        <label for="last-name-input" class="input-group-text">Last Name</label>
                                    </div>
                                    <div id="last-name-container" class="form-control">
                                        <input id="last-name-input" type="text" placeholder="Last name">
                                    </div>
                                </div>
                            </div>

                            <div class="wrap-for-input">
                                <div class="input-group" id="email-group">
                                    <div class="input-group-prepend">
                                        <label for="email-input" class="input-group-text">Email</label>
                                    </div>
                                    <div id="email-container" class="form-control">
                                        <input id="email-input" type="text" placeholder="Email">
                                    </div>
                                </div>
                            </div>

                            <div class="wrap-for-input">
                                <div class="input-group" id="phone-group">
                                    <div class="input-group-prepend">
                                        <label for="phone-input" class="input-group-text">Phone</label>
                                    </div>
                                    <div id="phone-container" class="form-control">
                                        <input id="phone-input" type="text" placeholder="Contact phone number">
                                    </div>
                                </div>
                            </div>

                            <div class="wrap-for-input">
                                <div class="input-group" id="social-id-group">
                                    <div class="input-group-prepend">
                                        <label for="social-id-input" class="input-group-text">Social id</label>
                                    </div>
                                    <div id="social-id-container" class="form-control">
                                        <input id="social-id-input" type="text" placeholder="Social id">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="input-group line-for-several-input">
                            <div class="input-group-prepend icon-for-card">
                                <div class="input-group-text">
                                            <span class="input-group-addon"><i class="fas fa-credit-card"
                                                                               id="card-provider"></i></span>
                                </div>
                            </div>
                            <div id="card-number-group" class="block-for-card-number">
                                <div id="card-number-container" class="form-control"></div>
                            </div>
                            <div id="card-expiration-group" class="block-for-expiration-card">
                                <div id="card-expiration-container" class="form-control"></div>
                            </div>
                            <div id="card-cvv-group" class="block-for-cvv-card">
                                <div id="card-cvv-container" class="form-control"></div>
                            </div>
                        </div>

                    </div>

                    <button type="submit" class="subscribe btn btn-lg btn-block" id="submit-button" disabled>
                        Pay $55
                    </button>

                    <div class="block-for-validate-errors">
                        <p id="errors" class="help-block animated"></p>
                    </div>
                    <div class="block-for-payment-errors">
                        <p class="payment-errors"></p>
                    </div>

                </form>
                <!-- CREDIT CARD FORM ENDS HERE -->
                <div class="success animated" style="display: none">
                    <div class="wrap-loading">
                        <div class="loading"></div>
                    </div>
                    <div class="body">
                        <div class="name query-info"></div>
                        <div class="email query-info"></div>
                        <div class="phone query-info"></div>
                        <div class="socialId query-info"></div>
                        <div class="token query-info"></div>
                    </div>
                    <div class="back-on-form1"><i class="fas fa-arrow-alt-circle-left"></i></div>
                </div>
            </div>
        </div>
    </div>
    <script src="./js/payme.js"></script>
@endsection
