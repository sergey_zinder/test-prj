
@extends('layout')
@section('title', 'New Sale')
@section('content')
    <form method="POST" action="{{ url('/') }}">
        @csrf
        <div class="flex-left">
            <label for="productName">Product Name</label>
            <input id="productName" name="productName" type="text" class="">
        </div>
        <div class="flex-left">
            <label for="price">Price</label>
            <input id="price" name="price" type="text" class="">
        </div>
        <div class="flex-left">
            <label for="currency">Currency</label>
            <select name="currency" id="currency">
                <option value="ILS" selected>ILS</option>
                <option value="USD">USD</option>
                <option value="EUR">EUR</option>
            </select>
        </div>
        <div class="flex-center">
            <input type="submit" value="Insert payment details"
                   style="width: 70%; background-color: cornflowerblue;">
        </div>
    </form>
@endsection
