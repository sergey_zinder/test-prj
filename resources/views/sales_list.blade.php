@extends('layout')
@section('title', 'Sales List')

@section('content')
    @if (empty($salesList))
        <h1>You have no any sales yet...</h1>
    @else
        <table>
            <tr>
                <th>Time</th>
                <th>Sale number</th>
                <th>Description</th>
                <th>Amount</th>
                <th>Currency</th>
            </tr>
            @foreach ($salesList as $sale)
                <tr>
                    <td>{{ \Carbon\Carbon::parse($sale['created_at'])->format('d/m/Y') }}</td>
                    <td>{{ $sale['sale_number'] }}</td>
                    <td>{{ $sale['description'] }}</td>
                    <td>{{ $sale['price'] }}</td>
                    <td>{{ $sale['currency'] }}</td>
                </tr>
            @endforeach
        </table>
    @endif
@endsection
