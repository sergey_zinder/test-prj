@extends('layout')
@section('title', 'New Sale')
@section('content')
    @if (!empty($error) || empty($url))
        <h1>Something went wrong...  {{ $error ? $error : '' }}</h1>
    @else
        <iframe src="{{ $url }}" style="width:520px; height:430px;">
    @endif
@endsection
