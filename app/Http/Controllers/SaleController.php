<?php

namespace App\Http\Controllers;

use App\Sales;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\View\View;
use Throwable;

class SaleController extends BaseController
{

    const PAYME_LANGUAGE = 'en';
    const PAYME_INSTALLMENTS = '1';


    /**
     * Get list of all existing sails
     * No pagination needed at this point
     *
     * @param Request $request
     * @return Application|Factory|View
     */
    public function index(Request $request) {
        $showPaymentForm = $request->get('p_form', false);

        return $showPaymentForm
            ? view('payment_form')
            : view('sales_list', ['salesList' => Sales::all()->toArray()]);
    }


    /**
     * Create new sale
     *
     * @param Request $request
     * @return Application|Factory|View
     */
    public function store(Request $request)
    {
        $url = '';
        $error = '';
        $input = $request->all();

        try {
            //try to get PayMe payment page data
            $paymePageData = $this->getPaymentPageData($input['price'], $input['currency'], $input['productName']);

            if (empty($paymePageData) || $paymePageData['status_code'] == '1') {
                $error = $paymePageData['status_error_details']
                    ? $paymePageData['status_error_details']
                    : 'Please try again later';
            } else {
                // save new sale into DB
                $sale = new Sales();
                $sale->description = $input['productName'];
                $sale->sale_number = $paymePageData['payme_sale_code'];
                $sale->price = $input['price'];
                $sale->currency = $input['currency'];
                $sale->save();

                // get payment page url
                $url = $paymePageData['sale_url'];
            }
        } catch (Throwable $exception) {
            $error = $exception->getMessage();
        }

        return view('payment_page', ['url' => $url, 'error' => $error]);
    }


    /**
     * Return form for new sale creation
     *
     * @return Application|Factory|View
     */
    public function create()
    {
        return view('new_sale');
    }


    /**
     * Get PayMe payment page data
     *
     * @param float $price
     * @param string $currency
     * @param string $productName
     * @return array
     */
    protected function getPaymentPageData(float $price, string $currency, string $productName): array
    {
        $PaymentPageDataArr = [];

        // get PayMe data from configuration
        $paymeEndpointUrl = config('app.payme_endpoint_url');
        $paymeEndpointKey = config('app.payme_endpoint_key');

        // call PayMe endpoint in order to get payment page data
        $response = Http::post($paymeEndpointUrl, [
            'language' => self::PAYME_LANGUAGE,
            'currency' => $currency,
            'sale_price' => $price * 100, // price should be converted to cents
            'product_name' => $productName,
            'installments' => self::PAYME_INSTALLMENTS,
            'seller_payme_id' => $paymeEndpointKey,
        ]);

        // if response OK - convert data to array and return
        if($response->successful()) {
            $PaymentPageDataArr = json_decode($response->body(), true);
        }

        return $PaymentPageDataArr;
    }
}
